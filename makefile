# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


# Commands
RM=rm -f
MKDIR=mkdir -p
LATEX2PDF=$(MKDIR) build && cd src && \
	pdflatex -output-format pdf -output-directory ../build
INKSCAPE=inkscape --without-gui
DIA=dia --nosplash

# Archive(s)
PACKAGE=dossier-capet-2017-proxy
SRC_FILES_TO_ARCHIVE=src/*.tex src/images/* makefile licences/* \
	.gitignore .editorconfig .gitlab-ci.yml


.PHONY: \
	default all \
	open-fr open-dossier-fr open-presentation-fr \
	dist zip zip-pdf zip-src \
	pdf pdf-fr dossier dossier-fr presentation presentation-fr images \
	clean clean-tmp clean-images clean-build \
	clean-archives clean-archives-tar \
	clean-archives-java clean-archives-php clean-archives-os \
	clean-archives-android clean-archives-windows \
	clean-archives-macos clean-archives-ios \
	clean-profiling clean-cmake \
	clean-ide clean-qt-creator clean-codeblocks \
	clean-git


default: pdf

all: pdf dist


open-fr: open-dossier-fr open-presentation-fr

open-dossier-fr: dossier-fr
	xdg-open build/dossier.fr.pdf &

open-presentation-fr: presentation-fr
	xdg-open build/presentation.fr.pdf &


dist: zip-src

zip: zip-pdf zip-src

zip-pdf: pdf
	@$(MKDIR) build
	zip build/pdf.zip -- build/*.pdf

zip-src:
	@$(MKDIR) build
	zip -r build/src.zip -- $(SRC_FILES_TO_ARCHIVE)


pdf: pdf-fr

pdf-fr: dossier-fr presentation-fr

dossier: dossier-fr

dossier-fr: build/dossier.fr.pdf

build/dossier.fr.pdf: \
		src/dossier*.fr.tex \
		src/preamble.fr.tex src/preamble_*.tex \
		.images.done
	$(LATEX2PDF) dossier.fr.tex
	$(LATEX2PDF) dossier.fr.tex

presentation: presentation-fr

presentation-fr: build/presentation.fr.pdf

build/presentation.fr.pdf: \
		src/presentation.fr.tex \
		src/preamble.fr.tex src/preamble_*.tex \
		.images.done
	$(LATEX2PDF) presentation.fr.tex
	$(LATEX2PDF) presentation.fr.tex


images: .images.done

.images.done : \
		build/images/creative-commons_by-sa_logo-88x31.pdf \
		build/images/internet.pdf \
		build/images/network-workgroup.pdf \
		build/images/modele-osi.pdf \
		build/images/keys.pdf \
		build/images/chair.pdf \
		build/images/wireshark-logo.pdf \
		build/images/computer.pdf \
		build/images/development.pdf \
		build/images/thinker.pdf \
		build/images/wireshark-screenshot-comments.png
	echo '' > .images.done

build/images/creative-commons_by-sa_logo-88x31.pdf: \
		src/images/creative-commons_by-sa_logo-88x31.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/creative-commons_by-sa_logo-88x31.pdf \
		src/images/creative-commons_by-sa_logo-88x31.svg

build/images/internet.pdf: \
		src/images/internet.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/internet.pdf \
		src/images/internet.svg

build/images/network-workgroup.pdf: \
		src/images/network-workgroup.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/network-workgroup.pdf \
		src/images/network-workgroup.svg

build/images/modele-osi.pdf: \
		src/images/modele-osi.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/modele-osi.pdf \
		src/images/modele-osi.svg

build/images/keys.pdf: \
		src/images/keys.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/keys.pdf \
		src/images/keys.svg

build/images/chair.pdf: \
		src/images/chair.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/chair.pdf \
		src/images/chair.svg

build/images/wireshark-logo.pdf: \
		src/images/wireshark-logo.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/wireshark-logo.pdf \
		src/images/wireshark-logo.svg

build/images/computer.pdf: \
		src/images/computer.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/computer.pdf \
		src/images/computer.svg

build/images/development.pdf: \
		src/images/development.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/development.pdf \
		src/images/development.svg

build/images/thinker.pdf: \
		src/images/thinker.svg
	@$(MKDIR) build/images
	$(INKSCAPE) \
		--export-pdf=build/images/thinker.pdf \
		src/images/thinker.svg

build/images/wireshark-screenshot-comments.png: \
		src/images/wireshark-screenshot-comments.xcf
	@$(MKDIR) build/images
	xcf2png \
		src/images/wireshark-screenshot-comments.xcf \
		-o build/images/wireshark-screenshot-comments.png


clean: clean-tmp clean-build clean-archives clean-profiling clean-cmake clean-ide
	cd src && make --directory=.. clean-tmp clean-latex

clean-tmp:
	$(RM) -rf -- \
		*~ .\#* \#* \
		*.swp *.swap *.SWP *.SWAP \
		*.bak *.backup *.BAK *.BACKUP \
		*.sav *.save *.SAV *.SAVE \
		*.autosav *.autosave \
		*.log *.log.* error_log* log/ logs/ \
		.cache/ .thumbnails/ \
		.CACHE/ .THUMBNAILS/

clean-images:
	$(RM) -rf -- build/images/ .img.done .images.done

clean-build:
	$(RM) -rf -- build/ Build/ BUILD/

clean-archives: \
		clean-archives-tar clean-archives-os \
		clean-archives-java clean-archives-php
	$(RM) -f -- \
		*.deb *.rpm \
		*.DEB *.RPM \
		*.gz *.bz2 *.lz *.lzma *.xz \
		*.GZ *.BZ2 *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar \
		*.ZIP *.7Z *.RAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-archives-tar:
	$(RM) -f -- *.tar *.tar.* *.tgz *.TGZ *.tbz2 *.TBZ2

clean-archives-java:
	$(RM) -f -- *.jar *.JAR

clean-archives-php:
	$(RM) -f -- *.phar *.PHAR

clean-archives-os: \
	clean-archives-android clean-archives-windows \
	clean-archives-macos clean-archives-ios

clean-archives-android:
	$(RM) -f -- *.apk *.APK

clean-archives-windows:
	$(RM) -f -- *.exe *.EXE *.msi *.MSI

clean-archives-macos:
	$(RM) -f -- *.dmg *.DMG

clean-archives-ios:
	$(RM) -f -- *.ipa *.IPA

clean-latex:
	$(RM) -f -- \
		*.pdf *.dvi \
		*.acn *.aux *.bcf *.cut *.fls *.glo *.ist *.lof *.nav *.out \
		*.run.xml *.snm *.toc *.vrb *.vrm *.xdy \
		*.fdb_latexmk *-converted-to.* \
		*.synctex *.synctex.xz \
		*.synctex.gz *.synctex.gzip \
		*.synctex.bz2 *.synctex.bzip \
		*.synctex.lz *.synctex.lzma \
		*.synctex.zip *.synctex.7z *.synctex.rar

clean-profiling:
	$(RM) -f -- callgrind.out.*

clean-cmake:
	$(RM) -rf -- \
		CMakeLists.txt.user CMakeCache.txt \
		CMakeFiles/ cmake_install.cmake \
		CPackConfig.cmake CPackSourceConfig.cmake \
		ctest.cmake CTest.cmake \
		CTestfile CTestFile \
		CTestfile.cmake CTestFile.cmake \
		CTestTestfile CTestTestFile \
		CTestTestfile.cmake CTestTestFile.cmake

clean-ide: clean-qt-creator clean-codeblocks

clean-qt-creator:
	$(RM) -f -- qmake_makefile *.pro.user

clean-codeblocks:
	$(RM) -f -- *.cbp *.CBP

clean-git:
	git clean -fdx
