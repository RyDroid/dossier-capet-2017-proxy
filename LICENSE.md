# Licences et droit d'auteur

## Ma production

Je veux que ce que j'ai produit serve au maximum de personnes
et je n'ai pas la volonté d'en tirer un bénéfice (autre que symbolique).
Je partage donc ma production pour le CAPET externe en 2017
sous des [licences libres](https://fr.wikipedia.org/wiki/Licence_libre)
avec [copyleft](https://fr.wikipedia.org/wiki/Copyleft).

- [Licence Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr),
  en version 3.0 et 4.0
- [Licence GNU de documentation libre](https://www.gnu.org/copyleft/fdl.fr.html),
  en version 1.3
- [Licence Art Libre](http://artlibre.org/licence/lal/),
  en version 1.3
- [Licence publique générale GNU](https://www.gnu.org/licenses/gpl.fr.html),
  en version 2.0 et 3.0

Les licences citées autorisent
l'utilisation pour tous les usages, la modification, et le partage.
Cela s'applique pour tous moyens (Internet, clé USB, etc)
et sa nature (centralisé, pair-à-pair, commercial, etc),
ainsi qu'avec tous formats
(code [LaTeX](https://www.latex-project.org/),
PDF, impression papier, etc),
que cela soit une version originale ou modifiée.
Les licences citées ne requièrent pas de
demander ou dire que vous utilisez, modifiez et/ou partagez
tout ou partie de ce document,
il y a néanmoins une clause d'attribution et de partage à l'identique.

Vous pouvez choisir la licence qui vous convient le plus
ou une licence compatible.
Il en est de même pour les numéros de version.
L'utilisation de plusieurs licences et plusieurs versions
est faite pour maximiser la compatibilité juridique.
En effet, [certaines licences sont incompatibles (avec d'autres)](https://en.wikipedia.org/wiki/License_compatibility),
c'est par exemple le cas de [la GPL avec la FDL](https://en.wikipedia.org/wiki/License_compatibility#GFDL_and_GPL)
et [la version 2.0 de la GPL avec sa version 3.0](https://www.gnu.org/licenses/gpl-faq.fr.html#v2v3Compatibility).
Veuillez noter que ce sont toutes des licences
avec [copyleft](https://www.gnu.org/copyleft/copyleft.fr.html)
(parfois traduit en "gauche d'auteur").

## Images

J'ai fait les captures d'écran de Wireshark.
Hormis les logos qu'elles contiennent,
elles font parti ma production,
et je les publie sous [la licence Creative Commons 0 version 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr).

### Images tierces

J'ai utilisé des images que je n'ai pas produites
et sur lesquelles je n'ai pas de droit d'auteur.
Elles ne font donc pas parti de ma production
et ne sont donc pas couvertes par la partie dédiée.

- [La chaise (`chair.svg`)](https://commons.wikimedia.org/wiki/File:Desk_chair_icon.svg)
  est sous [licence Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en).
- Des [icônes du "Tango Desktop Project"](http://tango.freedesktop.org/Tango_Icon_Library)
  de [freedesktop.org](https://fr.wikipedia.org/wiki/Freedesktop.org)
  qui sont dans le domaine public
  - [L'ordinateur (`computer.svg`)](https://commons.wikimedia.org/wiki/File:Computer.svg)
  - [La planète en réseau (`internet.svg`)](https://commons.wikimedia.org/wiki/File:Applications-internet.svg)
  - [Les ordinateurs inter-connectés (`network-workgroup.svg`)](https://commons.wikimedia.org/wiki/File:Network-workgroup.svg)
- Des [icônes du projet GNOME](https://commons.wikimedia.org/wiki/GNOME_Desktop_icons)
  - [`development.svg`](https://commons.wikimedia.org/wiki/File:Gnome-colors-emblem-development.svg)
  - [Les clés (`keys.svg`)](https://commons.wikimedia.org/wiki/File:Gnome-dialog-password.svg)
- Le logo de la licence Creative Commons BY-SA
- Le logo de [Snort](https://www.snort.org/)
- Le logo de [Varnish HTTP Cache](https://www.varnish-cache.org/)
- Le logo de [Wireshark](https://www.wireshark.org/)
  qui sous sous licence [GNU GPL version 2](https://www.gnu.org/licenses/gpl-2.0.fr.html) ou plus
  [d'après Wikipédia](https://commons.wikimedia.org/wiki/File:Wireshark_icon.svg)
- [`thinker.svg`](https://thenounproject.com/term/thinker/215431/)
  (par Gilbert Bages
  sous [Creative Commons Attribution 3.0 US](https://creativecommons.org/licenses/by/3.0/us/)
  sur [The Noun Project](https://thenounproject.com/))
  est [la statue "Le Penseur"](https://fr.wikipedia.org/wiki/Le_Penseur)
  par [Auguste Rodin](https://fr.wikipedia.org/wiki/Auguste_Rodin).
- [Le schéma du modèle OSI](https://commons.wikimedia.org/wiki/File:OSI_Model_v1.svg)
  est sous [la licence Creative Commons 0 version 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)
  (qui permet de mettre volontairement une création dans le domaine public).

## Références

Des documents de l'Éducation Nationale
(notamment des [bulletins officiels](https://fr.wikipedia.org/wiki/Bulletin_officiel_(France)))
ont été utilisés.
Il ne me semble pas qu'il y ait une licence explicite pour ceux-ci.
