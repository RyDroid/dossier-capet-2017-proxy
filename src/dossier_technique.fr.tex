\section{Partie scientifique et technique}

\subsection{Définition d'un proxy réseau}

Un proxy permet l'accès à un type de ressource, mais indirectement.
C'est un intermédiaire avec l'offreur réel de ressource(s).
L'usage d'un proxy pourrait paraitre parasitaire
(pourquoi rajouter un intermédiaire ?),
mais ce n'est pas le cas,
car un proxy offre au moins un service.
Le ou les services qu'un proxy offre constitue sa plus-value,
et donc la raison de son existence et son utilisation.

Un proxy réseau est un intermédiaire dans un réseau, comme Internet.
Parmi les services qu'un proxy de ce type peut offrir,
il y a :

\begin{itemize}
\item
  Filtrer des communications entrantes et/ou sortantes
  (pour un objectif de sécurité)
\item
  Garder au plus près des données qui pourraient être redemandées,
  pour pouvoir les resservir rapidement et économiser de la bande passante,
  ce qui peut être décrit de façon plus concise comme un système de cache
\item
  Anonymiser le demandeur réel d'une ressource
  vis-à-vis de l'offreur réel de cette dernière
\item
  Absorber une demande inhabituellement excessive,
  ce qui permet à l'offreur réel d'une ressource
  de ne pas avoir à gérer cette éventualité,
  et donc de ne pas directement avoir à gérer techniquement et financièrement
  ce service s'il n'opère pas le proxy ayant cette fonction
\item
  Enregistrer ce qui passe à travers lui ou au moins une partie pour :
  \begin{itemize}
  \item
    l'analyser en temps réel ou
    a posteriori si le besoin s'en fait sentir
  \item
    enregistrer un scénario de test qui pourra être rejoué
    pour vérifier que le fonctionnement réseau d'une application n'a pas changé
  \end{itemize}
\end{itemize}

\subsection{Proxy et chiffrement}

\subsubsection{Le chiffrement}

Le chiffrement consiste à transformer des données
via des principes mathématiques
pour faire en sorte qu'uniquement le ou les personne(s) légitime(s)
puisse(nt) lire les données originelles.
Le déchiffrement consiste en l'opération inverse :
transformer des données chiffrées en données claires (donc lisibles).
Chacune de ses opérations nécessite une clé,
qui est en réalité un ``grand'' nombre.
Il y a donc une clé de chiffrement pour chiffrer
et une clé de déchiffrement pour déchiffrer,
du moins conceptuellement puisque dans certains systèmes
(dits \href{https://fr.wikipedia.org/wiki/Cryptographie_sym\%C3\%A9trique}{symétriques})
une même clé a les 2 rôles.

Pour que ce genre de système soit efficace dans un système de communications,
il faut qu'une personne qui intercepte un flux chiffré
ne soit pas capable de retrouver le flux en clair,
du moins pas dans un temps moyen raisonnable
avec les capacités techniques de l'époque.
Pour cela, il faut un secret partagé entre les 2 personnes qui communiquent,
il s'agit de la clé de déchiffrement.
La clé de chiffrement peut être publique
si elle est distincte de la clé de déchiffrement,
c'est-à-dire dans le cas d'un système dit \href{https://fr.wikipedia.org/wiki/Cryptographie_asym\%C3\%A9trique}{asymétrique}.
Pour que le système soit efficace,
il faut que la clé de déchiffrement ne puisse pas être retrouvée
ni avec la clé de chiffrement,
ni avec une donnée dans sa forme claire et chiffrée,
ni avec la capacité de chiffrer n'importe quelle donnée.
Bien entendu, il faut aussi que
la clé de déchiffrement soit indispensable pour déchiffrer,
sinon cela signifie que l'on peut décrypter,
c'est-à-dire obtenir la donnée en clair à partir de sa version chiffrée
sans la clé de déchiffrement.

Pour des raisons de confidentialité,
le chiffrement est de plus en plus utilisé.
Il a un intérêt fort dans le cadre des télécommunications,
qui est d'ailleurs son principal champ d'application.
\href{https://fr.wikipedia.org/wiki/R\%C3\%A9v\%C3\%A9lations_d\%27Edward_Snowden}
{Les révélations d'Edward Snowden sur la surveillance massive par des États}
ont fortement amplifié cette tendance.
Les proxys reçoivent donc des données chiffrées,
ce qui n'est pas nécessairement sans conséquence
pour les opérations pour lesquelles ils ont été mis en place.

\subsubsection{La problématique du chiffrement pour un proxy}

Un proxy qui reçoit une donnée chiffrée sans la clé de déchiffrement
ne peut réaliser aucune opération pertinente en rapport avec cette dernière.
Or une donnée chiffrée pourrait contenir des informations
nécessaires au(x) service(s) qu'il est censé réaliser.
Pour les proxys, ainsi que celles et ceux qui les gèrent,
il y a 2 cas de figure :

\begin{itemize}
\item
  Le ou les opération(s) du proxy empêché(s) par le chiffrement
  est/sont une plus value intéressante mais non nécessaire(s).
  Dans ce cas, il suffit de ne pas traiter les données chiffrées.
\item
  Le ou les opérations du proxy impossible(s) à cause du chiffrement
  est/sont jugé(s) indispensable(s).
  Il va donc falloir d'une manière ou d'une autre contourner le chiffrement.
  Dans la suite de cette sous-sous-section,
  nous intéressons à ce cas.
\end{itemize}

Décrypter n'est pas possible,
à moins de trouver au moins une faille dans le système de chiffrement.
Mais pour cela, il faut avoir
de fortes connaissances en mathématiques et du temps,
ainsi que la ``chance'' qu'il y ait au moins une faille critique.
C'est donc réservé aux chercheurs et chercheuses,
et par extension à celles et ceux qui ont les moyens de s'offrir leurs services.

Une solution simple est de bannir ce qui est chiffré.
Cela a le mérite d'être aisé techniquement et donc peu couteux.
Néanmoins c'est mauvais pour la sécurité.
De plus, cette méthode n'est possible que
si tous les services jugés indispensables
auxquels les machines derrière le proxy veulent accéder
acceptent de communiquer sans chiffrement.
Pour ce qui est des services Internet publics,
c'est de moins en moins le cas
(puisque les fournisseurs de services ont intérêt
à avoir une forte sécurité pour ne pas perdre la confiance du public),
or leur usage est de plus en plus courant,
et usuellement nommé sous l'appellation marketing de ``\foreign{cloud}''.

Une autre possibilité est de déchiffrer.
Mais pour cela, il faut que le chiffrement ne soit plus du client au service final
(puisque le proxy n'a pas la clé de déchiffrement du dit service),
mais du client au proxy puis du proxy au service final.
Pour faire cela, il faut que les clients aient une clé de chiffrement du proxy.
C'est facile à mettre en place dans le cas d'ordinateurs contrôlés
par l'organisation qui gère le proxy réseau.
En effet, il suffit en effet de rajouter
un \href{https://fr.wikipedia.org/wiki/X.509}{certificat X.509},
ce qui est aisé sur tous les systèmes d'exploitation grand public.
Cependant, ce n'est pas suffisant,
puisque le service final donne les informations pour chiffrer les données pour lui.
Il faut donc que le proxy modifie ces informations
pour les remplacer par les siennes.
Ainsi, une fois que la configuration a été faite,
c'est transparent pour les utilisatrices et utilisateurs.

Pour certains protocoles de communications,
il est possible de faire un proxy non transparent.
Avec ce genre de proxy,
il est aisé de savoir qu'on se connecte à un service tiers,
puisque c'est fait explicitement au niveau du protocole.
Si une ressource fait une référence à une autre du même service,
il est nécessaire de modifier la référence pour la faire pointer vers le proxy,
ce qui nécessite de la puissance de calcul
et une logique applicative dépendante du protocole.
Pour contourner le chiffrement,
il n'y a pas de problème
puisque c'est au proxy de communiquer avec le service final
et non plus au client.
Ce genre de proxy est particulièrement utilisé pour le Web,
par exemple pour contourner la censure avec un miroir
(il en existe par exemple de nombreux pour
\href{http://www.numerama.com/magazine/31502-the-pirate-bay-sera-bloque-mais-pas-ses-nouveaux-miroirs.html}
{\foreign{The Pirate Bay}})
ou retrouver une page qui n'est plus en ligne
(comme le propose \href{https://archive.org/web/}{la \foreign{Wayback Machine}}
de \href{https://archive.org/}{l'\foreign{Internet Archive}}).

\subsection{Proxy filtrant}

\subsubsection{Introduction sur les proxys filtrants}

Internet est, comme sa dénomination l'indique, un réseau de réseaux.
Concrètement, c'est le résultat de plusieurs réseaux inter-connectés,
pas nécessairement directement les uns aux autres.
Chaque réseau est administré par un opérateur,
qui dans sa tâche n'a pas nécessairement les mêmes besoins que ces camarades.
L'un peut par exemple souhaiter ne pas permettre l'accès à des applications
qu'il juge inapproprié vis-à-vis de la raison de la mise en place du réseau.
Par exemple, une école aura typiquement pour volonté
d'interdire les sites web pornographiques.
L'autre peut vouloir garder un ou des services en interne,
car il(s) n'aurai(en)t (supposément) aucun intérêt au dehors
ou plus communément pour des raisons de sécurité
(moins il y a de portes moins il y a de probabilité qu'au moins une soit mal fermée).

Certains de ses besoins peuvent
se concrétiser en restriction(s) d'inter-connexion,
que ce soit dans le sens des entrées et/ou dans celui des sorties,
il s'agit là de filtrage.
Pour le mettre en œuvre, ou plutôt le faire faire,
l'opérateur d'un réseau utilise un ou des éléments techniques
qui sont entre un réseau qu'il administre et un ou plusieurs autres.
Le(s) ``élément(s) technique(s)'' sera/seront
nommé(s) ici proxy(s) filtrant(s).

Tout filtre logique a besoin de facteur(s),
pour déterminer s'il doit laisser passer ou non.
Informatiquement cela se matérialise par un algorithme
et un jeu de données si le filtre est paramétrable.
Dans le cadre du réseau,
un proxy filtrant écoute une interface
(qui va lui fournir des éléments à filtrer)
et émet sur une interface (ce qui a passé le filtre avec succès),
l'interface de réception et d'émission pouvant être la même.

Pour filtrer un réseau informatique,
on peut s'appuyer sur 2 grands types de données :
les métadonnées et les données applicatives.
Les métadonnées permettent techniquement de communiquer
et sont bien souvent étrangères à un usager lambda,
c'est une sorte de mal nécessaire.
Les données applicatives sont inhérentes à la logique métier de l'application,
donc techniquement les plus abstraites
et qui peuvent informer plus précisément sur l'usage du réseau.

\subsubsection{Proxy filtrant se basant sur les ``métadonnées''}

Ce que l'on a nommé métadonnées est très souvent codé de la même manière.
Cela se traduit par l'omni-présent TCP/IP
et UDP/IP dans une moindre mesure que le premier.
Faire un filtre sur IP, UDP, et/ou TCP
va toucher à peu près toutes les communications réseaux
dans la vaste majorité des réseaux,
et le tout simplement techniquement.
Les filtres s'appuyant sur des propriétés de ces protocoles
(notamment l'adresse source et destination pour IP,
ainsi que le numéro de port pour UDP et TCP)
sont donc légion.
Puisque les données en question sont techniques,
il y a peu de problèmes de vie privée à les manipuler
et ces derniers ne sont pas compréhensibles intuitivement
(pour beaucoup de personnes),
il y a donc un consensus sur la proportionnalité de leur usage
et par extension de la légalité de ce dernier.

Un filtre basé sur les métadonnées est efficace
pour protéger les serveurs derrière le proxy
et restreindre le béotien en informatique.
En effet, un individu ayant des connaissances en informatique
pourra aisément contourner ces restrictions,
particulièrement s'il a un accès avec des droits illimités
à une machine derrière le proxy.
Il lui suffit d'installer un serveur dans un réseau non restreint par le proxy
et de le configurer comme une passerelle réseau
qui s'appuie sur un protocole accepté par le proxy
et aura pour rôle de désencapsuler les requêtes et encapsuler les réponses.
Cela se fait couramment en TCP/IP avec le port 443
(utilisé usuellement pour le HTTPS, c'est-à-dire le web chiffré),
en transportant d'autres protocoles dans la charge utile
au lieu de transporter des ressources web,
charge utile que le serveur émettra et
qui encapsulera la réponse dans la charge utile de TCP (au-dessus d'IP)
en ayant indiqué le port 443.
Face à ce genre d'individu et si on le juge utile,
il faut donc utiliser au moins une autre méthode
(en théorie sans enfreindre la légalité).

\subsubsection{Proxy filtrant se basant sur des statistiques}

Des méthodes de filtrage plus élaborées que celles précédemment présentées
s'appuient sur des statistiques.
En effet, les paquets qu'un protocole engendre ont souvent une taille similaire.
De plus, l'intervalle de temps entre 2 paquets d'un même protocole
peut être significativement différent (en moyenne)
de celui d'autres protocoles.
Il est donc possible de faire du filtrage avec des statistiques,
à condition d'avoir un référenciel statistique sur les protocoles,
ou au moins sur les plus courants que l'opérateur du réseau souhaite filtrer.

En informatique, ce référentiel est un jeu de données.
Les statistiques sur les protocoles n'ont, dans la vaste majorité,
pas été pensés par celles et ceux qui les ont conçus.
Il faut donc bien souvent analyser l'usage des protocoles,
ou utiliser des raisonnements mathématiques théoriques,
pour obtenir des statistiques.
Il y a des bases statistiques déjà faites
que les fournisseurs de proxy peuvent fournir.
On peut aussi souhaiter en créer une,
par exemple par économie
si celles qui nous conviendraient sont jugées trop couteuses
ou pour en avoir une personnalisée
vis-à-vis de l'usage des protocoles sur un réseau précis.
Si on choisit cette dernière possibilité,
on peut par exemple s'appuyer sur des algorithmes d'apprentissage,
qui apprennent une fois pour toutes ou qui apprennent toute leur ``vie'',
et sont un type d'algorithme d'intelligence artificielle
qui est en ce moment à la mode
(c'est par exemple ce genre de technique qui a été utilisé par
\href{https://www.nextinpact.com/news/99021-des-echecs-au-jeu-go-quand-intelligence-artificielle-depasse-homme.htm}
{AlphaGo qui a battu un célèbre joueur de go})
(mais il y a des raisons objectives à son succès contemporain
que nous ne développerons pas ici).

Cette technique de filtrage peut être contournée.
Pour cela, il faut émettre plus de paquets que nécessaire
(ce qui est très rarement prévu par le protocole),
en essayant de se rapprocher du modèle statistique
pour un ou des protocoles autorisés.
Mais cela implique qu'il faut avoir la connaissance du modèle
ou faire des hypothèses sur celui-ci,
et avoir suffisamment de bande passante pour le surplus de paquets.

\subsubsection{Proxy filtrant se basant sur le DPI applicatif}

La méthode la plus élaborée et complexe,
mais également la plus couteuse à implémenter
est probablement le DPI au niveau applicatif.
C'est l'abréviation \foreign{Deep Package Inspection},
qui signifie en français inspection en profondeur de paquet.
Concrètement le DPI applicatif fait usage des protocoles applicatifs,
ce qui est particulièrement intrusif (en terme de vie privée).

Puisque le DPI applicatif s'appuie sur le protocole applicatif,
il faut qu'il le comprenne,
ce qui nécessite d'avoir un algorithme de filtrage
pour chaque protocole que l'on veut supporter,
or il y en a tellement
qu'il est impossible \textit{in concreto} de les supporter exhaustivement,
il faut donc se limiter à une partie.
Cela peut par exemple servir à
discriminer seulement certaines ressources d'un site web
(en se basant sur l'entête HTTP)
ou des emails qui contiendraient un ou plusieurs mot-clés
(\href{https://www.theverge.com/2017/4/28/15474828/nsa-surveillance-snowden-collection-email-702}
{comme l'aurait fait la NSA}).

Néanmoins cette méthode est de moins en moins efficace en pratique.
En effet, pour être efficiente, il faut pouvoir lire en clair
les échanges dans le protocole applicatif.
Or ils sont de plus en plus chiffrés.
Le chiffrement peut être réalisé dans le protocole,
ce qui est une recommandation de
\href{https://ietf.org/}{l'IETF (\foreign{Internet Engineering Task Force})},
l'organisme qui s'occupe de faire la standardisation des protocoles Internet,
par exemple à travers les RFC
\href{http://www.bortzmeyer.org/7258.html}{7258} et
\href{http://www.bortzmeyer.org/7624.html}{7624}.
Il peut également être fait par encapsulation dans un protocole prévu à cet effet.
Cela se fait couramment via un VPN (\foreign{Virtual Private Network})
(qui est commun dans les collectifs de productions
se souciant de leurs données confidentielles
et chez les individus aspirant à ne pas être surveillables facilement)
ou un réseau d'anonymisation
(comme le célèbre \href{https://www.torproject.org/}{projet Tor}).

\subsection{Proxy tampon}

\subsubsection{Proxy pour le cache}

Il y a des ressources disponibles via un réseau
qui ne changent pas ou peu souvent.
C'est notamment le cas de certains types de ressources web,
un très bon exemple est les images
(bien que l'on pourrait objecter qu'il y en a qui sont générées automatiquement
et potentiellement différemment selon le visiteur,
mais c'est un cas fort peu commun actuellement).

Plutôt que de redemander une ressource déjà obtenue ou déja générée,
il peut être pertinent de la garder en mémoire (persistante ou temporaire).
On appelle cela un système de cache.
Cela nécessite bien entendu de la place dans la mémoire.
De plus, pour que cela ait un intérêt, il faut que
la vitesse d'accès de la mémoire en lecture pour une ressource
soit supérieure au temps d'accès de la même ressource par le réseau
ou au temps de génération de la ressource.

Les applications, qui sont susceptibles de
redemander par le réseau ou regénérer une ressource,
ont souvent un système de cache pour être plus efficaces
(en temps, en bande passante, en temps de calcul, etc).
L'exemple le plus commun est le navigateur web.
En effet, il n'aura échappé à aucun·e internaute
que les pages web d'un même site web se ressemblent souvent
(même thème graphique, même logo, etc).
Puisque les navigateurs web représentent une part notoire
(en terme d'usage) des applications utilisant Internet,
il y a un intérêt non négligeable à ce qu'ils aient un système de cache,
ce qui est le cas pour les plus connus du grand public
(comme Mozilla Firefox et Google Chrome).

Chaque application qui y a un intérêt
peut donc avoir son système de cache en local.
Mais si plusieurs applications ont besoin d'une même ressource
qui pourrait avoir un intérêt à être dans un cache,
il est préférable de mutualiser le système de cache
plutôt que d'en avoir plusieurs indépendants.
De même, si plusieurs internautes d'un même réseau
demandent ou pourraient demander
une même ressource récupérable par ledit réseau,
il y a un intérêt à avoir un système de cache commun.
C'est précisément le but d'un proxy réseau pour le cache.

Si celui-ci est transparent,
c'est-à-dire que les applications n'ont pas d'indication
qu'il y a un système de cache,
elles vont aussi nécessairement utiliser leurs systèmes de cache
pour celles qui en ont un.
Cela peut être perçu comme redondant, et c'est vrai (partiellement),
cependant cela a l'avantage de moins solliciter le proxy réseau
et de moins en dépendre.
De plus, les informations qui sont chiffrées de bout-en-bout
ne peuvent être déchiffrés par le proxy (du moins pas trivialement)
et sont différentes (dans leurs formes chiffrées)
pour chaque usager d'un système cryptographique asymétrique
(pour celles-ci il est donc inutile de les mettre en cache dans un proxy).

Ce type de proxy est couramment utilisé dans les organisations,
notamment pour le Web.
Parmi les implémentations de ce type de proxy,
on peut par exemple citer
\href{http://www.squid-cache.org/}{Squid} et
\href{https://www.varnish-cache.org/}{Varnish},
qui sont connus et libres.

\subsubsection{Proxy anonymisant}

Les réseaux transportent des données.
On peut souhaiter que certaines d'entre elles
ne soient pas ou difficilement rattachables à une ou plusieurs de nos identités.
C'est d'ailleurs un sujet de préoccupation croissante pour la population,
notamment depuis les révélations d'Edward Snowden
que des médias de masse ont fortement relayé (au début du moins).
Il y a donc un intérêt à ce qu'il y ait des moyens pour préserver
au moins en partie sa confidentialité
(\href{https://www.lemonde.fr/pixels/article/2015/03/27/l-onu-se-dote-d-un-rapporteur-special-sur-la-vie-privee_4602512_4408996.html}
{qui est nécessaire pour les libertés fondamentales})
lors de l'usage de réseau.

Un de ces moyens est le proxy réseau anonymisant.
Son rôle est de transformer les données techniques
des échanges à anonymiser par les siennes.
Cela implique qu'il va recevoir les requêtes à anonymiser,
ainsi que leurs potentielles réponses.
Un corollaire de ce fait est qu'il faut avoir confiance
pour le respect de la vie privée dans le proxy,
ou en tout plus que dans le ou les services
que l'utilisateur ou utilisatrice veut faire usage en étant anonyme.
De plus, puisqu'un proxy anonymisant s'occupe de données techniques,
il ne s'occupe pas des autres données,
et n'est donc pas suffisant dans bien des cas
pour maximiser son anonymat tout en continuant à utiliser le réseau
(on pense notamment au JavaScript fortement utilisé sur le Web).

Dans le cas d'Internet, un proxy anonymisant va a minima
changer l'adresse source des paquets à anonymiser par une des siennes,
tout en conservant au moins un moyen de faire la translation inverse.
C'est une opération très basique et courante qui a d'autres intérêts.
Elle est connue sous les abréviations de NAT et PAT.

Un NAT (\foreign{Network Address Translation})
change l'adresse source par une des siennes et garde la correspondance.
Cette méthode implique qu'il faut autant d'adresses
pour l'élément faisant du NAT
que d'adresses sources à anonymiser.
Cela peut être problématique en IPv4,
puisqu'\href{https://fr.wikipedia.org/wiki/\%C3\%89puisement_des_adresses_IPv4}
{il n'y a presque plus d'adresses publiques libres de ce type},
mais ne l'est pas en IPv6
puisqu'il y a $2^{16}$, soit 65536, fois plus d'adresses qu'en version 4.
De plus, ce n'est pas très efficace pour anonymiser
puisque l'adresse IP source est fixe lors d'une session,
ce qui fait une donnée constante et unique pour chaque communication
et cela peu importe le protocole au-dessus d'IP.

Un PAT (\foreign{Port Address Translation})
change le numéro de port au niveau de la couche transport
(en pratique celui de TCP ou UDP)
ainsi que l'adresse IP source par une des siennes
et retient la correspondance.
Cette technique ne nécessite qu'une adresse IP,
ce qui est particulièrement intéressant avec IPv4
(et explique son usage courant dans les routeurs Internet
notamment ceux des opérateurs commerciaux souvent nommés \foreign{box}).
De plus, puisque chaque application utilise
un numéro de port différent des autres applications,
le proxy modifie le numéro de port différemment pour chaque application.
Pour un besoin de vie privée,
il est fortement préférable que la translation (du numéro de port)
soit faite avec un algorithme qui inclut un aléa non déterministe,
sinon il serait possible de prédire la translation
à condition de connaitre l'algorithme de translation
et de génération d'aléa déterministe (s'il y en a un).
Pour maximiser l'anonymat avec cette technique,
il est souhaitable qu'un maximum d'internautes utilisent le proxy,
puisqu'une même addresse IP sera utilisée (indirectement)
par plusieurs ordinateurs.

Faire usage d'au moins le NAT ou le PAT est nécessaire,
mais il y a des raisons qui peuvent pousser à ne pas s'en contenter.
Tout d'abord, on peut craindre les intermédiaires techniques
pour lesquels on a besoin de leurs éléments techniques
pour échanger avec le proxy.
Pour ne pas à avoir besoin d'avoir confiance en eux
pour le respect de la confidentialité,
il faut qu'il soit possible de chiffrer la communication avec le proxy
de bout-en-bout (ce qui se fait souvent via le protocole TLS).
De plus, on peut se méfier du proxy.
Pour remédier à cela, on peut faire passer ses télécommunications
par plusieurs proxys successivement,
ainsi il n'y a que le premier qui a la connaissance de la réelle adresse IP
et le dernier qui connait le contenu en ``clair'' de la charge utile
(qui peut être chiffré de bout-en-bout avec le service final)
s'il y a eu un chiffrement de bout-en-bout pour chaque proxy.

Pour éviter de devoir faire une installation complexe pour utiliser
les 2 techniques complémentaires du paragraphe précédent,
il existe des solutions de type ``clé en main''.
La plus connue et utilisée s'appelle Tor
(pour \foreign{The Onion Router}
puisque le chiffrement successif de proxy en proxy
peut faire penser aux couches d'un oignon)
qui est parfois confondu avec \foreign{Tor Browser}
(un navigateur web avec des réglages favorables à la confidentialité
et qui utilise le réseau d'anonymisation Tor).
Il faut néanmoins noter que Tor ne traite (actuellement) que les flux TCP/IP
(à cause de son usage de TLS qui se base sur TCP),
qui sont de loin les plus courants,
mais ce qui peut tout de même être problématique pour certains usages
(comme la voix sur IP qui utilise souvent UDP).

\subsubsection{Proxy contre le déni de service}

Une des convivialités qu'a permis les réseaux de télécommunications
est l'accès à des services 24 heures sur 24 et 7 jours sur 7, bref tout le temps
(et sans travail humain hormis la conception et la mise en œuvre).
Ce genre de service est opéré par un serveur
(qui peut être matériel
mais qui n'est présentement presque plus qu'exclusivement logiciel)
qui tente de répondre aux requêtes qu'il reçoit.
Comme un serveur humain, un serveur informatique a
une limite dans sa capacité de traitement quantitative.
Si elle est dépassée,
soit le serveur ne répondra qu'à une partie des requêtes,
soit il s'arrêtera de fonctionner jusqu'au redémarrage.
On appelle ce phénomène un déni de service.
Pour éviter ou au moins limiter le risque que cela arrive,
on peut mettre un proxy avant le serveur
qui va s'occuper d'absorber le potentiel surplus de charge.

Un proxy contre le déni de service est souvent le seul service réseau
(en ne prenant pas en compte ceux dédiés à l'administration)
sur une machine physique.
Il en est ainsi pour qu'il ait le maximum de capacité d'absorption de charge.
De plus, il est souvent dimensionné d'une manière supérieure
au(x) service(s) qu'il est censé protéger du déni de service.

S'il y a un nombre de requêtes supérieur à ce que peut traiter un service,
un proxy contre le déni de service a 2 solutions pour faire barrage.
La première est de jeter les paquets surnuméraires.
Cela a le mérite d'être simple et ne pas demander de ressource matériel.
La seconde solution est de garder en cache
les requêtes et les envoyer au serveur
quand la charge sera strictement inférieure au maximum.
Le maximum peut être soit défini en dur
dans le programme ou dans un fichier de configuration,
soit déduit par le proxy par exemple via un algorithme d'apprentissage.
Bien entendu, ce cache nécessite de la mémoire,
et de la rapidité puisque les usagers pourraient fuir
un service qu'ils jugeraient trop lent.
Puisqu'un client s'attend souvent à avoir une réponse
en un temps qui ne dépasse pas un seuil maximal,
il est généralement pertinent de ne garder en cache que temporairement
les requêtes de trop, du moins pour la gestion dans l'urgence,
puisqu'à long terme il peut être pertinent de tenter de comprendre
pourquoi il y a eu plus de requêtes qu'habituellement
(mais on utilisera pour cela une mémoire moins rapide).

Il est possible d'utiliser les 2 solutions avec un même proxy.
Un proxy peut passer d'une solution à l'autre en fonction d'événements.
Par exemple, si le cache venait à être presque plein,
il pourrait être pertinent de passer en mode ``tout jeter'',
jusqu'à ce que la charge devienne gérable par le service.
Un proxy peut également utiliser une solution
pour certains types de requêtes ou
en fonction de la source ou de la destination,
et l'autre solution pour le reste des requêtes.
Cette technique peut par exemple servir pour favoriser
certains internautes et/ou services,
mais cela peut être perçu comme une violation du principe d'égalité
(en théorie une valeur fondamentale en France)
et donc de la neutralité d'Internet.

Les attaques de dénis de service peuvent être massives qualitativement
(comme cela a été le cas pour
\href{https://twitter.com/olesovhcom/status/779297257199964160}
{OVH en 2016 avec une attaque de plus de 1.5Tbps}).
La majorité des organisations n'ont pas
les moyens techniques (et leurs préalables financiers)
pour résister à de grosses attaques.
Si elles fournissent des services reposant sur un ou quelques ordinateurs
(contrairement à des services massivement distribués
dont l'extrême est les services pair-à-pair),
elles ne peuvent donc pas résister toutes seules.
Elles doivent donc accepter d'être vulnérables
à des attaques de dénis de service
(dont les conséquences sont souvent fâcheuses,
comme une perte de ventes ou d'image,
mais elles sont très rarement critiques,
puisqu'il s'agit juste de rendre indisponible un service,
ce qui ne peut pas entrainer la perte de données
et un redémarrage est en principe facile)
ou utiliser un service (de proxy) d'une organisation qui a plus de moyens
pour contrer ce type d'attaque.
Une entreprise célèbre qui est spécialisée dans ce domaine est
\href{http://sebsauvage.net/rhaa/index.php?2012/01/23/13/42/15-cloudflare-le-syndrome-akismet}{CloudFlare}.

\subsection{Proxy enregistreur}

\subsubsection{Introduction sur les proxys enregistreurs}

Puisqu'un proxy réseau est un intermédiaire technique
pour un client ou un serveur,
il peut techniquement enregistrer les communications
qui passent par lui ou une partie (de celles-ci).
En effet, un proxy enregistreur peut agir comme une caméra de surveillance :
tout enregistrer pour une ou des exploitations
en temps réel ou postérieure(s).
Cela peut avoir plusieurs intérêts.

Il faut noter que ce n'est pas forcément légal.
De plus, la loi définit des durées de conservation maximale
pour certains types de données.
Elle impose également une durée minimale et obligatoire
d'enregistrement de certaines informations.
C'est par exemple le cas des données de connexion en France,
mais cela est remis en cause par
\href{https://www.nextinpact.com/news/86929-pourquoi-directive-sur-conservation-donnees-est-invalidee.htm}
{l'arrêt \foreign{Digital Rights Ireland} de la CJUE}
(\foreign{Cours de Justice de l'Union Européenne})
\href{https://exegetes.eu.org/posts/tele2/}
{qui estime que ce genre de pratique est attentatoire aux libertés fondamentales}
quand cela est pratiqué sur les communications d'individu a priori innocents.

\subsubsection{Proxy pour analyser le trafic}

Le trafic réseau peut être révélateur d'usages abstraits
(à comprendre ici du point de vue d'un·e internaute
et non de celui d'un·e technicien·ne).
Des raisons peuvent pousser à essayer de de les trouver et de les comprendre.

Une de celles-ci est la compréhension d'attaques faites via le réseau,
voire la réduction au moins partielle de celles-ci.
L'exploitation peut se faire en temps réel,
mais cela nécessite une certaine puissance de calcul,
et un faible temps pour prendre une décision
(ne rien faire, émettre un avertissement, bloquer une partie du trafic, etc),
puisque dans le cas contraire cela ralentirait significativement le réseau
et serait perçu comme une gène par certaines personnes l'utilisant.
Les enregistrements peuvent aussi être analysés
juste sur intervention humaine
(par exemple pour une enquête),
mais il faut pour cela les avoir préalablement stockés
et donc avoir une infrastructure de stockage adéquate
avec de préférence une autre pour la réplication des données.
Dans ce cas, des outils d'analyse réseau et de filtrage réseau
non automatiques sont utilisés,
comme le célèbre \href{https://www.wireshark.org/}{Wireshark}
(qui dispose d'une interface graphique),
et/ou des bibliothèques de programmation dédiées à l'analyse réseau,
comme \href{http://secdev.org/projects/scapy/}{Scapy}
pour \href{https://www.python.org/}{le langage Python}.

Certains États pratiquent la surveillance de masse pour diverses raisons
(lutte contre le terrorisme et le crime organisé,
protection des intérêts économiques,
maintien de la paix publique, concurrence économique,
prévention de la prolifération des armes de destruction massive, etc).
Pour cela, ils utilisent techniquement des proxys.
On peut faire remarquer que
\href{https://wiki.laquadrature.net/Sp\%C3\%A9cial:MaLangue/PJL_relatif_au_renseignement}{la Loi Renseignement de 2015}
a poussé à de vives ``discussions'' sur l'usage de proxys pour cela,
qui ont couramment été nommés
\href{https://www.lemonde.fr/pixels/article/2015/04/01/loi-sur-le-renseignement-la-boite-noire-reste-obscure_4607264_4408996.html}{``boites noires''}.
Dans ce genre d'usage et quand il s'agit d'individus,
le proxy doit se baser sur des signaux faibles (donc peu fiables)
(qui sont censés être révélateurs d'un potentiel danger
vis-à-vis d'un ou plusieurs modèles de menaces)
pour conserver des informations,
puisqu'il serait bien trop couteux de tout sauvegarder
et également trop long de faire une analyse plus poussée sur beaucoup de données.

Il faut noter qu'un proxy pour analyser le trafic
peut servir pour plusieurs raisons et avoir divers rôles.
Un exemple courant et libre de ce type de proxy est
le logiciel \href{http://www.snort.org/}{Snort}.
Celui-ci peut entre autre
enregistrer le trafic totalement ou partiellement
(en obéissant à des règles programmables)
(d'où le logo d'un cochon avec un gros nez renifleur)
et détecter des attaques courantes (comme la recherche de ports ouverts)
en prévenant via une alerte quand c'est le cas.

\subsubsection{Proxy pour rejouer une session}

En programmation, il est recommandé de faire des tests unitaires.
Le but est d'avoir du code stable
(ce qui revient à vouloir le moins de bogues possibles)
et de ne pas avoir de régression
(c'est-à-dire qu'une nouvelle version ne doit pas entrainer
le mauvais fonctionnement de ce qui fonctionnait bien avant).
Concrètement chaque test unitaire appelle
une partie du code avec potentiellement un ou des paramètres
et vérifie que le résultat correspond à ce qui était attendu.

On peut appliquer ce principe pour vérifier le code d'une application réseau.
Cependant cela ne vérifie pas le fonctionnement du logiciel
tel qu'il va être utilisé \textit{in concreto}.
De plus, pour tester concrètement une application réseau,
il faut lui envoyer un flux réseau
et potentiellement répondre adéquatement à ce qu'elle enverra.

Il est possible de faire des tests unitaires réseaux,
en démarrant l'application
puis en comparant ce que l'on attendait d'elle
et ce qu'elle a envoyé ou pas comme réponses à des stimuli
(c'est-à-dire des requêtes par le réseau)
qui lui ont été engendrés préalablement.
On peut humainement créer les stimuli (via la programmation)
et déterminer les réponses attendues.
Dans le cadre de tests de non régression,
on peut aussi utiliser l'application
en enregistrant les informations réseaux qui lui sont liées
(celles qui lui ont été envoyées et celles qu'elle a envoyé),
pour rejouer ultérieurement la séquence des envois
et comparer avec la séquence des réponses obtenues précédemment.

L'enregistrement des séquences réseaux,
que l'on peut regrouper sous l'appellation de session réseau,
est faite dans la majorité des cas par un proxy réseau.
En effet, il est possible d'enregistrer la session au niveau du client réseau
pour tester un serveur réseau et donc de se passer d'un proxy
(comme le fait HPE TruClient
dans ses versions \foreign{Light} et \foreign{Standalone}).
Parmi les proxys réseaux enregistreurs, il y a par exemple
\href{https://guide.blazemeter.com/hc/en-us/articles/207420545-BlazeMeter-Proxy-Recorder-Mobile-and-web}
{\foreign{BlazeMeter Proxy Recorder}},
\href{http://www.neotys.com/recorder.html?lng=fr}{\foreign{NeoLoad Recorder}},
et celui de \href{http://clif.ow2.org/}{CLIF}.

Le proxy s'occupe uniquement de l'enregistrement de la session
et de l'export de celle-ci dans un format compréhensible
(souvent en se basant sur XML, JSON, ou YAML).
Ce format devra être interprétable par un logiciel capable de rejouer la session.
Pour cela, on utilise souvent un logiciel de test de charge,
comme \href{https://jmeter.apache.org/}{Apache JMeter},
\href{http://gatling.io/}{Gatling},
ou \href{http://clif.ow2.org/}{CLIF}.
