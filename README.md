# Documents pour le CAPET externe en option ingénierie informatique

## Présentation

Pour le CAPET externe, il faut faire un dossier et une présentation de celui-ci.
Le dossier doit avoir une partie technique
puis une partie pédagogique qui s'appuie sur la première.
Les 2 parties doivent avoir une taille similaire.

Je suis Nicola Spanti.
J'ai tenté en 2017 le CAPET externe
dans la section sciences industrielles de l’ingénieur
en option ingénierie informatique.
Mes résultats de l'écrit m'ont permis de passer l'"oral".
Pour cet "oral", il fallait faire le dossier et la présentation.

Il n'y a rien de secret dans mon dossier et dans ma présentation.
Je n'ai malheureusement pas trouvé un exemple publique et gratuit.
En effet, je trouvais que les consignes étaient très vagues.
J'ai eu la chance d'avoir un exemple pour le dossier
par quelqu'un qui a fait et réussi le CAPET dans un autre domaine de science dure.
Pour les prochains et prochaines,
je me suis donc dit qu'il pourrait être utile d'avoir un exemple.
Je partage donc ce que j'ai produit, profitez en !

Cependant ne vous méprenez pas.
Ce n'est pas un exemple officiel.
C'est ce qu'un candidat a produit
(en s'appuyant sur un exemple non public d'une personne
qui a réussi le concours dans une autre discipline).
De plus, sachez que je n'ai pas été accepté.
De mon ressenti, cela est du à l'épreuve de "mise en situation professionnelle"
(dans laquelle j'ai été fort mauvais),
mais il est possible que je me trompe.

Pour les curieux et curieuses,
j'ai fait un DUT informatique puis une école d'ingénieurs en informatique.
En 2017, c'était ma dernière année d'école d'ingénieurs.
Je considérais ne pas avoir de problème en informatique.
Étant donné mon parcours,
j'étais un béotien dans les autres sciences de l'ingénieur.
L'épreuve de "mise en situation professionnelle"
n'avait quasiment rien d'informatique technique,
certes elle était sur ordinateur,
mais les compétences utiles à sa réussite me paraissait plus liées
au domaine de l'électronique
(un peu d'informatique, de la physique,
de la simulation 3D et de la composition matérielle d'un robot, etc),
ce qui pour moi explique ma piètre perfomance à cette épreuve.

## Logiciels utilisés

- [LaTeX](https://www.latex-project.org/)
  - [La classe beamer](https://fr.wikibooks.org/wiki/LaTeX/Faire_des_pr%C3%A9sentations#Beamer)
    pour la présentation
- [Inkscape](https://inkscape.org/fr/) pour les images vectorielles
- [GIMP](https://www.gimp.org/fr/) pour les images matricielles
- [make](https://fr.wikipedia.org/wiki/Make)
- [xcftools](https://packages.debian.org/stable/graphics/xcftools)

Ce sont tous des logiciels libres et gratuits.
Si vous ne connaissez pas les concepts
de [logiciel libre](https://www.gnu.org/philosophy/free-sw.fr.html)
et [propriétaire / privateur](https://www.gnu.org/proprietary/proprietary.fr.html),
il y a de nombreuses explications sur Internet.
Il y a notamment [le site web de la FSF (Free Software Foundation)](https://fsf.org/)
et [celui du projet GNU](https://www.gnu.org/home.fr.html).
[La présentation de Richard Stallman au TEDx en anglais simple peut être un très bon moyen de comprendre l'essentiel en 14 minutes](https://www.fsf.org/blogs/rms/20140407-geneva-tedx-talk-free-software-free-society).
Pour celles et ceux ayant beaucoup de mal avec l'anglais
et voulant quelque chose de rapide,
[le même monsieur a fait une explication vidéo plus courte et sous-titré en français](https://www.gnu.org/education/education.fr.html)
qui va encore plus à l'essentiel.
Si vous connaissez l'expression "open-source",
sachez que [logiciel libre et logiciel open-source ne sont pas strictement identiques](https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html).
